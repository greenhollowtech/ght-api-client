<?php

namespace GHT\ApiClient\Tests\Entity;

use GHT\ApiClient\Entity\OauthConnector;

/**
 * Exercises the OauthConnector.
 */
class OauthConnectorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Verify that the OAuth2 Connector can be instantiated with only a host.
     */
    public function testConstructUnauthenticated()
    {
        // Get the OAuth2 Connector
        $connector = new OauthConnector('http://test.greenhollowtech.com');

        // Verify the OAuth2 Connector properties
        $this->assertEquals('http://test.greenhollowtech.com', $connector->getHost());
        $this->assertNull($connector->getToken());
    }

    /**
     * Verify that the OAuth2 Connector can be instantiated with a host and
     * authentication token.
     */
    public function testConstructAuthenticated()
    {
        // Get the OAuth2 Connector
        $connector = new OauthConnector('http://test.greenhollowtech.com', 'testToken');

        // Verify the OAuth2 Connector properties
        $this->assertEquals('http://test.greenhollowtech.com', $connector->getHost());
        $this->assertEquals('testToken', $connector->getToken());
    }

    /**
     * Verify that the OAuth2 Connector does not have authentication when the
     * token is not supplied.
     */
    public function testHasAuthenticationNoToken()
    {
        // Get the OAuth2 Connector without setting a token
        $connector = new OauthConnector('http://test.greenhollowtech.com');

        // Verify the authentication status
        $this->assertFalse($connector->hasAuthentication());
    }

    /**
     * Verify that the OAuth2 Connector does not have authentication when not all
     * credentials are supplied.
     */
    public function testHasAuthenticationWithToken()
    {
        // Get the OAuth2 Connector with the token set
        $connector = new OauthConnector('http://test.greenhollowtech.com', 'testToken');

        // Verify the authentication status
        $this->assertTrue($connector->hasAuthentication());
    }

    /**
     * Verify that a non-secure OAuth2 host can be detected.
     */
    public function testIsSecureWithHttp()
    {
        // Get the OAuth2 Connector
        $connector = new OauthConnector('http://test.greenhollowtech.com');

        // Verify the non-secure status
        $this->assertFalse($connector->isSecure());
    }

    /**
     * Verify that a secure OAuth2 host can be detected.
     */
    public function testIsSecureWithHttps()
    {
        // Get the OAuth2 Connector
        $connector = new OauthConnector('https://test.greenhollowtech.com');

        // Verify the secure status
        $this->assertTrue($connector->isSecure());
    }
}
