<?php

namespace GHT\ApiClient\Tests\Entity;

use GHT\ApiClient\Entity\ApiConnector;

/**
 * Exercises the ApiConnector.
 */
class ApiConnectorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Verify that the API Connector can be instantiated with only a host.
     */
    public function testConstructUnauthenticated()
    {
        // Get the API Connector
        $connector = new ApiConnector('http://test.greenhollowtech.com');

        // Verify the API Connector properties
        $this->assertEquals('http://test.greenhollowtech.com', $connector->getHost());
        $this->assertNull($connector->getUser());
        $this->assertNull($connector->getKey());
        $this->assertNull($connector->getSecret());
    }

    /**
     * Verify that the API Connector can be instantiated with a host and
     * authentication credentials.
     */
    public function testConstructAuthenticated()
    {
        // Get the API Connector
        $connector = new ApiConnector('http://test.greenhollowtech.com', 'testUser', 'testKey', 'testSecret');

        // Verify the API Connector properties
        $this->assertEquals('http://test.greenhollowtech.com', $connector->getHost());
        $this->assertEquals('testUser', $connector->getUser());
        $this->assertEquals('testKey', $connector->getKey());
        $this->assertEquals('testSecret', $connector->getSecret());
    }

    /**
     * Verify that the API Connector does not have authentication when not all
     * credentials are supplied.
     */
    public function testHasAuthenticationNotEnoughCredentials()
    {
        // Get the API Connector without setting a secret
        $connector = new ApiConnector('http://test.greenhollowtech.com', 'testUser', 'testKey');

        // Verify the authentication status
        $this->assertFalse($connector->hasAuthentication());
    }

    /**
     * Verify that the API Connector can determine it has authentication when
     * all credentials are supplied.
     */
    public function testHasAuthenticationWithCredentials()
    {
        // Get the API Connector with all credentials set
        $connector = new ApiConnector('http://test.greenhollowtech.com', 'testUser', 'testKey', 'testSecret');

        // Verify the authentication status
        $this->assertTrue($connector->hasAuthentication());
    }

    /**
     * Verify that a non-secure API host can be detected.
     */
    public function testIsSecureWithHttp()
    {
        // Get the API Connector
        $connector = new ApiConnector('http://test.greenhollowtech.com');

        // Verify the non-secure status
        $this->assertFalse($connector->isSecure());
    }

    /**
     * Verify that a secure API host can be detected.
     */
    public function testIsSecureWithHttps()
    {
        // Get the API Connector
        $connector = new ApiConnector('https://test.greenhollowtech.com');

        // Verify the secure status
        $this->assertTrue($connector->isSecure());
    }
}
