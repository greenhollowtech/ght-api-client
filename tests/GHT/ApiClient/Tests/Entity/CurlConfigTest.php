<?php

namespace GHT\ApiClient\Tests\Entity;

use GHT\ApiClient\Entity\CurlConfig;

/**
 * Exercises the CurlConfig.
 */
class CurlConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Verify that the CurlConfig can be instantiated with default options.
     */
    public function testConstructWithDefaultOptions()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array('test_config' => 'test value'));

        // Verify the CurlConfig default options
        $this->assertEquals(array('test_config' => 'test value'), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that the CurlConfig can be instantiated without default options.
     */
    public function testConstructWithoutDefaultOptions()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig();

        // Verify the CurlConfig default options
        $this->assertEquals(array(), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that option keys are processed to lowercase.
     */
    public function testConstructOptionsKeyCasing()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_case' => 'this will get zapped',
            'test_config' => 'test value',
            'TEST_CASE' => 'test case',
        ));

        // Verify the CurlConfig default options
        $this->assertEquals(array('test_config' => 'test value', 'test_case' => 'test case'), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that option keys are processed to remove curlopt prefix.
     */
    public function testConstructOptionsWithCurloptPrefix()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'curlopt_test_config' => 'test value',
            'CURLOPT_OTHER_CONFIG' => 'other value',
        ));

        // Verify the CurlConfig default options
        $this->assertEquals(array('test_config' => 'test value', 'other_config' => 'other value'), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that option keys are processed to ignore reserved keys.
     */
    public function testConstructOptionsWithReservedKeys()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'customrequest' => 'ignored',
            'httpheader' => 'ignored',
            'postfields' => 'ignored',
            'writefunction' => 'ignored',
        ));

        // Verify the CurlConfig default options
        $this->assertEquals(array('test_config' => 'test value'), $curlConfig->getDefaultOptions());
    }

    /**
     * Verify that override option keys are processed as well.
     */
    public function testOverrideOptions()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig();
        $curlConfig->setOverrideOptions(array(
            'test_config' => 'test value',
            'CURLOPT_OTHER_CONFIG' => 'other value',
            'customrequest' => 'ignored',
        ));

        // Verify the CurlConfig override options
        $this->assertEquals(array('test_config' => 'test value', 'other_config' => 'other value'), $curlConfig->getOverrideOptions());
    }

    /**
     * Verify that the default and override options can be merged.
     */
    public function testGetOptions()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'other_config' => 'other value',
        ));
        $curlConfig->setOverrideOptions(array(
            'another_config' => 'another value',
            'other_config' => 'override value',
        ));

        // Verify the CurlConfig merged options
        $this->assertEquals(
            array(
                'test_config' => 'test value',
                'other_config' => 'override value',
                'another_config' => 'another value',
            ),
            $curlConfig->getOptions()
        );
    }

    /**
     * Verify that the default and override options can be merged and returned
     * keyed for cURL.  Invalid CURLOPT_* constant names should be ignored.
     */
    public function testGetCurlOptions()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'connecttimeout' => 20,
            'timeout' => 20,
        ));
        $curlConfig->setOverrideOptions(array(
            'connecttimeout' => 10,
            'other_config' => 'invalid',
        ));

        // Verify the CurlConfig merged options
        $this->assertEquals(
            array(
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
            ),
            $curlConfig->getCurlOptions()
        );
    }

    /**
     * Verify that some options can be ignored.
     */
    public function testGetOptionsWithIgnored()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'other_config' => 'other value',
        ));
        $curlConfig->setOverrideOptions(array(
            'another_config' => 'another value',
            'other_config' => 'override value',
        ));

        // Verify the CurlConfig merged-sans-ignored options
        $this->assertEquals(
            array('test_config' => 'test value'),
            $curlConfig->getOptions(array('another_config', 'other_config'))
        );
    }

    /**
     * Verify that some options can be ignored and returned keyed for cURL.
     */
    public function testGetCurlOptionsWithIgnored()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'connecttimeout' => 20,
            'timeout' => 20,
        ));
        $curlConfig->setOverrideOptions(array(
            'connecttimeout' => 10,
            'other_config' => 'invalid',
        ));

        // Verify the CurlConfig merged-sans-ignored options
        $this->assertEquals(
            array(CURLOPT_CONNECTTIMEOUT => 10),
            $curlConfig->getCurlOptions(array('timeout'))
        );
    }

    /**
     * Verify that option names to be ignored are processed.
     */
    public function testGetOptionsWithIgnoredProcessed()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'other_config' => 'other value',
        ));
        $curlConfig->setOverrideOptions(array(
            'another_config' => 'another value',
            'other_config' => 'override value',
        ));

        // Verify the CurlConfig merged-sans-ignored options
        $this->assertEquals(
            array('test_config' => 'test value'),
            $curlConfig->getOptions(array('ANOTHER_CONFIG', 'curlopt_other_config'))
        );
    }

    /**
     * Verify that option names to be ignored are processed, and options are
     * returned keyed for cURL.
     */
    public function testGetCurlOptionsWithIgnoredProcessed()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'connecttimeout' => 20,
            'timeout' => 20,
        ));
        $curlConfig->setOverrideOptions(array(
            'connecttimeout' => 10,
            'other_config' => 'invalid',
        ));

        // Verify the CurlConfig merged-sans-ignored options
        $this->assertEquals(
            array(CURLOPT_CONNECTTIMEOUT => 10),
            $curlConfig->getCurlOptions(array('CURLOPT_TIMEOUT'))
        );
    }

    /**
     * Verify that a single option can be gotten.
     */
    public function testGetOption()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'other_config' => 'other value',
        ));

        // Verify the option
        $this->assertEquals('other value', $curlConfig->getOption('other_config'));
    }

    /**
     * Verify that a single option can be gotten respecting the override.
     */
    public function testGetOptionWithOverride()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array(
            'test_config' => 'test value',
            'other_config' => 'other value',
        ));
        $curlConfig->setOverrideOptions(array(
            'another_config' => 'another value',
            'other_config' => 'override value',
        ));

        // Verify the option
        $this->assertEquals('override value', $curlConfig->getOption('other_config'));
    }

    /**
     * Verify that getting a single option that doesn't exist returns null.
     */
    public function testGetOptionDoesNotExist()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array('test_config' => 'test value'));

        // Verify the option doesn't exist
        $this->assertNull($curlConfig->getOption('other_config'));
    }

    /**
     * Verify that getting a single option processes the requested option name.
     */
    public function testGetOptionProcessedName()
    {
        // Get the CurlConfig
        $curlConfig = new CurlConfig(array('test_config' => 'test value'));

        // Verify the option
        $this->assertEquals('test value', $curlConfig->getOption('CURLOPT_TEST_CONFIG'));
    }
}
