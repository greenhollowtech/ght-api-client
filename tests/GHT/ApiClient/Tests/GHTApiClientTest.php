<?php

namespace GHT\ApiClient\Tests;

use GHT\ApiClient\Entity\ApiConnector;
use GHT\ApiClient\Entity\CurlConfig;
use GHT\ApiClient\Entity\OauthConnector;
use GHT\ApiClient\GHTApiClient;
use phpmock\phpunit\PHPMock;

/**
 * Exercises the ApiClient.
 */
class GHTApiClientTest extends \PHPUnit_Framework_TestCase
{
    use PHPMock;

    /**
     * @var array
     */
    protected $mockFunctions;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // Configure all the expected PHP functions
        $this->mockFunctions = array();
        foreach (array(
            'curl_close',
            'curl_exec',
            'curl_getinfo',
            'curl_init',
            'curl_setopt',
            'fseek',
            'fwrite',
            'tmpfile',
        ) as $functionName) {
            $this->mockFunctions[$functionName] = $this->getFunctionMock('GHT\ApiClient', $functionName);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->mockFunctions);
    }

    /**
     * Configure the API Client.
     *
     * @param boolean $authenticated Set to true to include credentials.
     * @param boolean $oauth Set to true if the credentials are for OAuth2.
     *
     * @return GHTApiClient
     */
    public function configureApiClient($authenticated = false, $oauth = false)
    {
        $connector = $authenticated
            ? ($oauth
                ? new OauthConnector('https://test.greenhollowtech.com', 'testToken')
                : new ApiConnector('https://test.greenhollowtech.com', 'testUserName', 'testApiKey', 'testApiSecret')
            )
            : new ApiConnector('http://test.greenhollowtech.com')
        ;

        return new GHTApiClient($connector);
    }

    /**
     * Verify that a DELETE request is made as expected.
     */
    public function testDelete()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array())))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the DELETE custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'DELETE');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        $hasContentLengthHeader = false;
                        foreach ($value as $header) {
                            // Will set data length header
                            if (preg_match('/^Content-Length:/', $header)) {
                                $hasContentLengthHeader = true;
                            }
                            // Will not set auth header
                            if (preg_match('/^Authorization:/', $header)) {
                                return false;
                            }
                        }

                        return $hasContentLengthHeader;
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the DELETE request
        $apiClient = $this->configureApiClient();
        $deleteData = array('test' => 'data');
        $result = $apiClient->delete('/test/delete', $deleteData);
        $this->assertEquals('[]', $result);
        $this->assertEquals(200, $apiClient->getResponseCode());
    }

    /**
     * Verify that a GET request is made as expected.
     */
    public function testGet()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // GET method, no custom request option will be set
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return false;
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        foreach ($value as $header) {
                            // Will set neither auth nor data length headers
                            if (preg_match('/^(Authorization|Content-Length):/', $header)) {
                                return false;
                            }
                        }
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the GET request
        $apiClient = $this->configureApiClient();
        $result = $apiClient->get('/test/status');
        $this->assertEquals('{"data":"test data"}', $result);
    }

    /**
     * Verify that the response code is captured and can be returned.
     */
    public function testGetResponseCode()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;

        // Make the GET request
        $apiClient = $this->configureApiClient();
        $result = $apiClient->get('/test/status');
        $this->assertEquals(200, $apiClient->getResponseCode());
    }

    /**
     * Verify that cURL options can be set.
     */
    public function testGetWithConfiguredOptions()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_setopt']->expects($this->exactly(3))
            ->withConsecutive(
                array($this->anything(), CURLOPT_RETURNTRANSFER, $this->anything()),
                array($this->anything(), CURLOPT_HTTPHEADER, $this->anything()),
                array($this->anything(), CURLOPT_CONNECTTIMEOUT, 10)
            )
        ;

        // Make the GET request
        $apiClient = $this->configureApiClient();
        $apiClient->setCurlConfig(new CurlConfig(array('connecttimeout' => 10)));
        $result = $apiClient->get('/test/status');
    }

    /**
     * Verify that a GET request streamed to a file is made as expected.
     */
    public function testGetToFile()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // A callable value will be set
                    if (is_callable($value)) {
                        return true;
                    }

                    // GET method, no custom request option will be set
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return false;
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        foreach ($value as $header) {
                            // Will set neither auth nor data length headers
                            if (preg_match('/^(Authorization|Content-Length):/', $header)) {
                                return false;
                            }
                        }
                    }

                    return true;
                })
            )
        ;

        // The response will be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->once())
            ->will($this->returnValue('testFileResource'))
        ;
        $this->mockFunctions['fseek']->expects($this->once());

        // Make the GET request
        $apiClient = $this->configureApiClient();
        $result = $apiClient->getToFile('/test/status');
        $this->assertEquals('testFileResource', $result);
    }

    /**
     * Verify that using reserved keys in the request data throws an exception.
     */
    public function testGetWithReservedKeys()
    {
        $this->setExpectedException(
            'InvalidArgumentException', 'Data uses reserved keys: "api-url", "api-key"'
        );

        // Make the GET request, triggering the exception
        $apiClient = $this->configureApiClient(true);
        $apiClient->get('/test/status', array(
            'test' => 'data',
            'api-url' => 'www.test.com',
            'api-key' => 'something-completely-different',
        ));
    }

    /**
     * Verify that a POST request is made as expected.
     */
    public function testPost()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        $hasContentLengthHeader = false;
                        foreach ($value as $header) {
                            // Will set data length header
                            if (preg_match('/^Content-Length:/', $header)) {
                                $hasContentLengthHeader = true;
                            }
                            // Will not set auth header
                            if (preg_match('/^Authorization:/', $header)) {
                                return false;
                            }
                        }

                        return $hasContentLengthHeader;
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request
        $apiClient = $this->configureApiClient();
        $postData = array('test' => 'data');
        $result = $apiClient->post('/test/status', $postData);
        $this->assertEquals('{"data":"test data"}', $result);
    }

    /**
     * Verify that a POST request is made as expected with authentication hash.
     */
    public function testPostAuthenticatedWithHash()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        $hasAuthHeader = false;
                        $hasContentLengthHeader = false;
                        foreach ($value as $header) {
                            // Will not set a bearer token auth header
                            if (preg_match('/^Authorization: Bearer/', $header)) {
                                return false;
                            }
                            // Will set both auth and data length headers
                            if (preg_match('/^Authorization:/', $header)) {
                                $hasAuthHeader = true;
                            }
                            if (preg_match('/^Content-Length:/', $header)) {
                                $hasContentLengthHeader = true;
                            }
                        }

                        return ($hasAuthHeader && $hasContentLengthHeader);
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request
        $apiClient = $this->configureApiClient(true);
        $postData = array('test' => 'data');
        $result = $apiClient->post('/test/status', $postData);
        $this->assertEquals('{"data":"test data"}', $result);
    }

    /**
     * Verify that a POST request is made as expected with OAuth2
     * authentication.
     */
    public function testPostAuthenticatedWithOauth()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        $hasAuthHeader = false;
                        $hasContentLengthHeader = false;
                        foreach ($value as $header) {
                            // Will set a bearer token auth header
                            if (preg_match('/^Authorization: Bearer/', $header)
                            ) {
                                $hasAuthHeader = true;
                            }
                            // Will set data length header
                            if (preg_match('/^Content-Length:/', $header)) {
                                $hasContentLengthHeader = true;
                            }
                        }

                        return ($hasAuthHeader && $hasContentLengthHeader);
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request
        $apiClient = $this->configureApiClient(true, true);
        $postData = array('test' => 'data');
        $result = $apiClient->post('/test/status', $postData);
        $this->assertEquals('{"data":"test data"}', $result);
    }

    /**
     * Verify that a POST request is made as expected with an upload file for
     * PHP >= 5.5.0.  Test is configured to work regardless of version.
     */
    public function testPostFileUploadPhpFiveFive()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array())))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value) && !isset($value['file'])) {
                        $hasContentTypeHeader = false;
                        foreach ($value as $header) {
                            // Will set the content type as multipart form data
                            if (preg_match('/^Content-Type:/', $header)) {
                                $hasContentTypeHeader = ($header === 'Content-Type: multipart/form-data');
                            }

                            // Will set neither auth nor data length headers
                            if (preg_match('/^(Authorization|Content-Length):/', $header)) {
                                return false;
                            }
                        }

                        return $hasContentTypeHeader;
                    }

                    return true;
                })
            )
        ;
        $classExistsFunction = $this->getFunctionMock('GHT\ApiClient', 'class_exists');
        $classExistsFunction->expects($this->any())
            ->with($this->equalTo('CURLFile'))
            ->will($this->returnValue(true))
        ;
        $getClassFunction = $this->getFunctionMock('GHT\ApiClient', 'get_class');
        $getClassFunction->expects($this->once())
            ->with($this->equalTo(new \stdClass()))
            ->will($this->returnValue('CURLFile'))
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request - test object will be interpreted as a cURL
        // file object by the mocked functions
        $apiClient = $this->configureApiClient();
        $postData = array('file' => new \stdClass());
        $result = $apiClient->post('/test/upload', $postData);
        $this->assertEquals('[]', $result);
    }

    /**
     * Verify that a POST request is made as expected with an upload file for
     * PHP < 5.5.0.  Test is configured to work regardless of version.
     */
    public function testPostFileUploadPhpFiveFour()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array())))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value) && !isset($value['file'])) {
                        $hasContentTypeHeader = false;
                        foreach ($value as $header) {
                            // Will set the content type as multipart form data
                            if (preg_match('/^Content-Type:/', $header)) {
                                $hasContentTypeHeader = ($header === 'Content-Type: multipart/form-data');
                            }

                            // Will set neither auth nor data length headers
                            if (preg_match('/^(Authorization|Content-Length):/', $header)) {
                                return false;
                            }
                        }

                        return $hasContentTypeHeader;
                    }

                    return true;
                })
            )
        ;
        $classExistsFunction = $this->getFunctionMock('GHT\ApiClient', 'class_exists');
        $classExistsFunction->expects($this->any())
            ->with($this->equalTo('CURLFile'))
            ->will($this->returnValue(false))
        ;
        $getClassFunction = $this->getFunctionMock('GHT\ApiClient', 'get_class');
        $getClassFunction->expects($this->never());

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request
        $apiClient = $this->configureApiClient();
        $postData = array('file' => '@/tmp/test.png;filename=test.png');
        $result = $apiClient->post('/test/upload', $postData);
        $this->assertEquals('[]', $result);
    }

    /**
     * Verify that a POST request streamed to a file is made as expected.
     */
    public function testPostToFile()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // A callable value will be set
                    if (is_callable($value)) {
                        return true;
                    }

                    // Will set the POST custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'POST');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        foreach ($value as $header) {
                            // Will set neither auth nor data length headers
                            if (preg_match('/^(Authorization|Content-Length):/', $header)) {
                                return false;
                            }
                        }
                    }

                    return true;
                })
            )
        ;

        // The response will be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->once())
            ->will($this->returnValue('testFileResource'))
        ;
        $this->mockFunctions['fseek']->expects($this->once());

        // Make the POST request
        $apiClient = $this->configureApiClient();
        $result = $apiClient->postToFile('/test/status');
        $this->assertEquals('testFileResource', $result);
    }

    /**
     * Verify that a PUT request is made as expected.
     */
    public function testPut()
    {
        // Set the called function expectations
        $this->mockFunctions['curl_init']->expects($this->once());
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode(array('data' => 'test data'))))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;
        $this->mockFunctions['curl_close']->expects($this->once());
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // No callable value will be set
                    if (is_callable($value)) {
                        return false;
                    }

                    // Will set the PUT custom request option
                    if (is_string($value) && in_array($value, array('DELETE', 'GET', 'POST', 'PUT'))) {
                        return ($value === 'PUT');
                    }

                    // If the headers array, validate
                    if (is_array($value)) {
                        $hasContentLengthHeader = false;
                        foreach ($value as $header) {
                            // Will set data length header
                            if (preg_match('/^Content-Length:/', $header)) {
                                $hasContentLengthHeader = true;
                            }
                            // Will not set auth header
                            if (preg_match('/^Authorization:/', $header)) {
                                return false;
                            }
                        }

                        return $hasContentLengthHeader;
                    }

                    return true;
                })
            )
        ;

        // The response will not be streeamed to a file
        $this->mockFunctions['tmpfile']->expects($this->never());

        // Make the POST request
        $apiClient = $this->configureApiClient();
        $putData = array('test' => 'data');
        $result = $apiClient->put('/test/status', $putData);
        $this->assertEquals('{"data":"test data"}', $result);
    }
}
