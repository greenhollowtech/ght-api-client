<?php

namespace GHT\ApiClient\Entity;

/**
 * Interface for the abstract base connector entity.
 */
interface ConnectorInterface
{
    /**
     * The constructor.
     *
     * @param string $host The target API host.
     */
    public function __construct($host = null);

    /**
     * Get the host.
     *
     * @return string
     */
    public function getHost();

    /**
     * Check if all the required credentials are present for an authenticated
     * connection.
     *
     * @return boolean
     */
    public function hasAuthentication();

    /**
     * Check if the host is via SSL.
     *
     * @return boolean
     */
    public function isSecure();

    /**
     * Set the host.
     *
     * @param string $host The host.
     *
     * @return \GHT\ApiClient\Entity\ConnectorInterface
     */
    public function setHost($host);
}
