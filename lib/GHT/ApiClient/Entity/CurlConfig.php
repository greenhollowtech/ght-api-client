<?php

namespace GHT\ApiClient\Entity;

/**
 * Entity for configuring cURL.
 */
class CurlConfig implements CurlConfigInterface
{
    /**
     * @var array
     */
    const RESERVED_OPTIONS = array(
        'customrequest',
        'httpheader',
        'postfields',
        'writefunction',
    );

    /**
     * @var array
     */
    protected $defaultOptions;

    /**
     * @var array
     */
    protected $overrideOptions;

    /**
     * The constructor.
     *
     * @param array $options The default options.
     */
    public function __construct(array $options = array())
    {
        $this->setDefaultOptions($options);
        $this->overrideOptions = array();
    }

    /**
     * Get the options, compiled from the default and override options, minus
     * any given option names, with keys set as cURL option names.
     *
     * @param array $ignoreOptions The option names to ignore.
     *
     * @return array
     */
    public function getCurlOptions(array $ignoreOptions = array())
    {
        $options = $this->getOptions($ignoreOptions);

        foreach (array_keys($options) as $key) {
            $value = $options[$key];
            unset($options[$key]);
            $key = $this->processCurlKey($key);
            try {
                if ($constant = constant($key)) {
                    $options[$constant] = $value;
                }
            }
            catch (\Exception $e) {
                // Do nothing
            }
        }

        return $options;
    }

    /**
     * Get the defaultOptions.
     *
     * @return array
     */
    public function getDefaultOptions()
    {
        return $this->defaultOptions;
    }

    /**
     * Get an option value.
     *
     * @param string $option The option name.
     *
     * @return mixed
     */
    public function getOption($option)
    {
        $option = $this->processKey($option);

        return isset($this->overrideOptions[$option])
            ? $this->overrideOptions[$option]
            : (isset($this->defaultOptions[$option]) ? $this->defaultOptions[$option] : null)
        ;
    }

    /**
     * Get the options, compiled from the default and override options, minus
     * any given option names.
     *
     * @param array $ignoreOptions The option names to ignore.
     *
     * @return array
     */
    public function getOptions(array $ignoreOptions = array())
    {
        $options = array_merge($this->defaultOptions, $this->overrideOptions);
        $ignoreOptions = $this->processOptions(array_flip($ignoreOptions));

        return array_diff_key($options, $ignoreOptions);
    }

    /**
     * Get the overrideOptions.
     *
     * @return array
     */
    public function getOverrideOptions()
    {
        return $this->overrideOptions;
    }

    /**
     * Process a key name back to a cURL option name.
     *
     * @param string $key The key.
     *
     * @return string
     */
    protected function processCurlKey($key)
    {
        return sprintf('CURLOPT_%s', strtoupper($key));
    }

    /**
     * Process a key name.
     *
     * @param string $key The key.
     *
     * @return string
     */
    protected function processKey($key)
    {
        return str_replace('curlopt_', '', strtolower($key));
    }

    /**
     * Clean out any reserved options and ensure all keys are cased properly.
     *
     * @param array $options The options to process.
     *
     * @return array
     */
    protected function processOptions(array $options)
    {
        foreach (array_keys($options) as $key) {
            $value = $options[$key];
            unset($options[$key]);
            $key = $this->processKey($key);
            if (!in_array($key, self::RESERVED_OPTIONS)) {
                $options[$key] = $value;
            }
        }

        return $options;
    }

    /**
     * Set the defaultOptions.
     *
     * @param array $defaultOptions The default options.
     *
     * @return \GHT\ApiClient\Entity\CurlConfig
     */
    public function setDefaultOptions(array $defaultOptions = array())
    {
        $this->defaultOptions = $this->processOptions($defaultOptions);

        return $this;
    }

    /**
     * Set the overrideOptions.
     *
     * @param array $overrideOptions The override options.
     *
     * @return \GHT\ApiClient\Entity\CurlConfig
     */
    public function setOverrideOptions(array $overrideOptions = array())
    {
        $this->overrideOptions = $this->processOptions($overrideOptions);

        return $this;
    }
}
