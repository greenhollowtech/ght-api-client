<?php

namespace GHT\ApiClient\Entity;

/**
 * Interface for the cURL configuration entity.
 */
interface CurlConfigInterface
{
    /**
     * The constructor.
     *
     * @param array $options The default options.
     */
    public function __construct(array $options = array());

    /**
     * Get the options, compiled from the default and override options, minus
     * any given option names, with keys set as cURL option names.
     *
     * @param array $ignoreOptions The option names to ignore.
     *
     * @return array
     */
    public function getCurlOptions(array $ignoreOptions = array());

    /**
     * Get the defaultOptions.
     *
     * @return array
     */
    public function getDefaultOptions();

    /**
     * Get an option value.
     *
     * @param string $option The option name.
     *
     * @return mixed
     */
    public function getOption($option);

    /**
     * Get the options, compiled from the default and override options, minus
     * any given option names.
     *
     * @param array $ignoreOptions The option names to ignore.
     *
     * @return array
     */
    public function getOptions(array $ignoreOptions = array());

    /**
     * Get the overrideOptions.
     *
     * @return array
     */
    public function getOverrideOptions();

    /**
     * Set the defaultOptions.
     *
     * @param array $defaultOptions The default options.
     *
     * @return \GHT\ApiClient\Entity\CurlConfigInterface
     */
    public function setDefaultOptions(array $defaultOptions = array());

    /**
     * Set the overrideOptions.
     *
     * @param array $overrideOptions The override options.
     *
     * @return \GHT\ApiClient\Entity\CurlConfigInterface
     */
    public function setOverrideOptions(array $overrideOptions = array());
}
