<?php

namespace GHT\ApiClient\Entity;

/**
 * Interface for the API connector.
 */
interface ApiConnectorInterface extends ConnectorInterface
{
    /**
     * The constructor.
     *
     * @param string $host The target API host.
     * @param string $user The API user name.
     * @param string $key The API key.
     * @param string $secret The secret that is used to create an authorization hash.
     */
    public function __construct($host = null, $user = null, $key = null, $secret = null);

    /**
     * Get the key.
     *
     * @return string
     */
    public function getKey();

    /**
     * Get the secret.
     *
     * @return string
     */
    public function getSecret();

    /**
     * Get the user.
     *
     * @return string
     */
    public function getUser();

    /**
     * Set the key.
     *
     * @param string $key The key.
     *
     * @return \GHT\ApiClient\Entity\ApiConnectorInterface
     */
    public function setKey($key);

    /**
     * Set the secret.
     *
     * @param string $secret The secret.
     *
     * @return \GHT\ApiClient\Entity\ApiConnectorInterface
     */
    public function setSecret($secret);

    /**
     * Set the user.
     *
     * @param string $user The user.
     *
     * @return \GHT\ApiClient\Entity\ApiConnectorInterface
     */
    public function setUser($user);
}
