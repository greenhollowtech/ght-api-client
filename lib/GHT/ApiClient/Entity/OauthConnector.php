<?php

namespace GHT\ApiClient\Entity;

/**
 * Entity for OAuth2 API connections.
 */
class OauthConnector extends Connector implements OauthConnectorInterface
{
    /**
     * @var string
     */
    protected $token;

    /**
     * The constructor.
     *
     * @param string $host The target API host.
     * @param string $token The OAuth2 token.
     */
    public function __construct($host = null, $token = null)
    {
        parent::__construct($host);

        $this->token = $token;
    }

    /**
     * Get the token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAuthentication()
    {
        return (!empty($this->token));
    }

    /**
     * Set the token.
     *
     * @param string $token The token.
     *
     * @return \GHT\ApiClient\Entity\OauthConnector
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}
