<?php

namespace GHT\ApiClient\Entity;

/**
 * Interface for the OAuth2 connector.
 */
interface OauthConnectorInterface extends ConnectorInterface
{
    /**
     * The constructor.
     *
     * @param string $host The target API host.
     * @param string $token The OAuth2 token.
     */
    public function __construct($host = null, $token = null);

    /**
     * Get the token.
     *
     * @return string
     */
    public function getToken();

    /**
     * Set the token.
     *
     * @param string $token The token.
     *
     * @return \GHT\ApiClient\Entity\OauthConnectorInterface
     */
    public function setToken($token);
}
