<?php

namespace GHT\ApiClient\Entity;

/**
 * Entity for authenticated or unauthenticated API connections.
 */
class ApiConnector extends Connector implements ApiConnectorInterface
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $user;

    /**
     * The constructor.
     *
     * @param string $host The target API host.
     * @param string $user The API user name.
     * @param string $key The API key.
     * @param string $secret The secret that is used to create an authorization hash.
     */
    public function __construct($host = null, $user = null, $key = null, $secret = null)
    {
        parent::__construct($host);

        $this->key = $key;
        $this->secret = $secret;
        $this->user = $user;
    }

    /**
     * Get the key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Get the secret.
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Get the user.
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAuthentication()
    {
        return (!empty($this->user) && !empty($this->key) && !empty($this->secret));
    }

    /**
     * Set the key.
     *
     * @param string $key The key.
     *
     * @return \GHT\ApiClient\Entity\ApiConnector
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Set the secret.
     *
     * @param string $secret The secret.
     *
     * @return \GHT\ApiClient\Entity\ApiConnector
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Set the user.
     *
     * @param string $user The user.
     *
     * @return \GHT\ApiClient\Entity\ApiConnector
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
