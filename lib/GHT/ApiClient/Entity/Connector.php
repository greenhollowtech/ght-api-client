<?php

namespace GHT\ApiClient\Entity;

/**
 * Abstract entity for API connections.
 */
abstract class Connector implements ConnectorInterface
{
    /**
     * @var string
     */
    protected $host;

    /**
     * The constructor.
     *
     * @param string $host The target API host.
     */
    public function __construct($host = null)
    {
        $this->setHost($host);
    }

    /**
     * Get the host.
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Check if all the required credentials are present for an authenticated
     * connection.
     *
     * @return boolean
     */
    public abstract function hasAuthentication();

    /**
     * Check if the host is via SSL.
     *
     * @return boolean
     */
    public function isSecure()
    {
        return preg_match('/^https/', $this->host) ? true : false;
    }

    /**
     * Set the host.
     *
     * @param string $host The host.
     *
     * @return \GHT\ApiClient\Entity\Connector
     */
    public function setHost($host)
    {
        $this->host = strtolower(trim(rtrim($host, '/')));

        return $this;
    }
}
