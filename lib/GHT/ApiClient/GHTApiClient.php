<?php

namespace GHT\ApiClient;

use GHT\ApiClient\Entity\ConnectorInterface;
use GHT\ApiClient\Entity\CurlConfigInterface;
use GHT\ApiClient\Entity\OauthConnector;

/**
 * Client for sending authenticated API requests.
 */
class GHTApiClient
{
    /**
     * @var \GHT\ApiClient\Entity\ConnectorInterface
     */
    protected $connector;

    /**
     * @var \GHT\ApiClient\Entity\CurlConfigInterface
     */
    protected $curlConfig;

    /**
     * @var array
     */
    protected $hashData;

    /**
     * @var bool
     */
    protected $isSandbox;

    /**
     * @var integer
     */
    protected $responseCode;

    /**
     * The constructor.
     *
     * @param \GHT\ApiClient\Entity\ConnectorInterface $connector The API connector.
     * @param boolean $isSandbox Set to true for sandbox mode.
     */
    public function __construct(ConnectorInterface $connector, $isSandbox = false)
    {
        $this->connector = $connector;
        $this->isSandbox = $isSandbox ? true : false;
    }

    /**
     * Request via the DELETE method to return the result as a JSON data string.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return string
     */
    public function delete($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'DELETE', $data);
    }

    /**
     * Request via the GET method to return the result as a JSON data string.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return string
     */
    public function get($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'GET', $data);
    }

    /**
     * Get the Authorization header.
     *
     * @param string $path The request path.
     * @param string $method The request method.
     * @param array $data The request data.
     *
     * @return string
     */
    protected function getAuthorizationHeader($path, $method = 'GET', $data = array())
    {
        // If the connnector doesn't have credentials, do nothing
        if (!$this->connector->hasAuthentication()) {
            return;
        }

        // If the connector uses OAuth2, return the token header
        if ($this->connector instanceof OauthConnector) {
            return sprintf(
                'Authorization: Bearer %s',
                $this->connector->getToken()
            );
        }

        // Compile the hash data
        $hashData = is_array($data) ? $data : array();
        $hashData = array_merge($hashData, array(
            'api-key' => $this->connector->getKey(),
            'api-method' => $method,
            'api-url' => $this->getRequestUrl($path),
        ));

        ksort($hashData);

        return sprintf(
            'Authorization: user=%s,key=%s,hash=%s',
            $this->connector->getUser(),
            $this->connector->getKey(),
            hash_hmac('sha256', json_encode($hashData), $this->connector->getSecret())
        );
    }

    /**
     * Get the request URL.
     *
     * @param string $path The API path to request from.
     * @param string $method The request method.
     * @param array $data The data to submit.
     *
     * @return string
     */
    protected function getRequestUrl($path, $method = 'GET', $data = array())
    {
        $data = is_array($data) ? $data : array();

        if ($data && $method === 'GET') {
            return sprintf('%s/%s?%s', $this->connector->getHost(), trim($path, '/'), http_build_query($data));
        }

        return sprintf('%s/%s', $this->connector->getHost(), trim($path, '/'));
    }

    /**
     * Get an array of reserved data keys.
     *
     * @return array
     */
    protected function getReservedKeys()
    {
        return array('api-key', 'api-method', 'api-url');
    }

    /**
     * Get the response code for the last request.
     *
     * @return integer
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Request via the GET method to return the result as a temporary file
     * handle resource.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return resource
     */
    public function getToFile($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'GET', $data, true);
    }

    /**
     * Check the request data for the presence of an upload file.  Attempts to
     * gracefully handle <> PHP 5.5.0 cURL file functionality.
     *
     * @param array $data The request data.
     *
     * @return boolean
     */
    protected function hasFile(array $data)
    {
        // PHP 5 >= 5.5.0, PHP 7
        if (class_exists('CURLFile')) {
            foreach (array_keys($data) as $key) {
                if (is_object($data[$key]) && get_class($data[$key]) === 'CURLFile') {
                    return true;
                }
            }
        }

        // PHP < 5.5.0
        foreach (array_keys($data) as $key) {
            if (is_string($data[$key]) && preg_match('/^\@.*;filename=/', $data[$key])) {
                return true;
            }
        }
    }

    /**
     * Make a request to the remote API endpoint via cURL.  If asFile is set to
     * true, the requested data is dumped into a temporary file, and the file
     * handle is returned.  Otherwise, the data is returned as a JSON encoded
     * string.  If in sandbox mode, SSL verification is suppressed, so test
     * requests can be made to a sandbox server with a self-signed certificate.
     *
     * @param string $path The request path.
     * @param string $method The request method.
     * @param array $data The request data.
     * @param boolean $asFile Stream data to a temporary file.
     *
     * @return string|resource
     */
    protected function makeRequest($path, $method = 'GET', $data = array(), $asFile = false)
    {
        // Check for reserved keys in the request data
        $reservedKeys = $this->getReservedKeys();
        if ($badKeys = array_intersect(array_keys($data), $reservedKeys)) {
            throw new \InvalidArgumentException(sprintf('Data uses reserved keys: "%s"', implode('", "', $badKeys)));
        }

        // Define the request
        $curl = curl_init($this->getRequestUrl($path, $method, $data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if ($this->connector->isSecure() && $this->isSandbox) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }

        // Set the method of the request and process any data
        $headers = array();
        if ($method !== 'GET') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

            if ($data) {
                if ($this->hasFile($data)) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    $headers[] = 'Content-Type: multipart/form-data';
                }
                else {
                    $jsonData = json_encode($data);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
                    $headers[] = 'Content-Type: application/json';
                    $headers[] = 'Content-Length: ' . strlen($jsonData);
                }
            }
            else {
                $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            }
        }
        else {
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }

        // Set the authorization header if needed
        if ($authorizationHeader = $this->getAuthorizationHeader($path, $method, $data)) {
            $headers[] = $authorizationHeader;
        }

        // Set the headers
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        // If streaming to a file, set the write function
        if ($asFile) {
            $tempFile = tmpfile();
            curl_setopt($curl, CURLOPT_WRITEFUNCTION, function($curl, $streamData) use (&$tempFile) {
                $length = fwrite($tempFile, $streamData);
                return $length;
            });
        }

        // Set any additional configured options
        if ($this->curlConfig instanceof CurlConfigInterface) {
            $options = $this->curlConfig->getCurlOptions();
            foreach ($options as $option => $optionValue) {
                curl_setopt($curl, $option, $optionValue);
            }
        }

        // Make the request
        $returnData = curl_exec($curl);
        $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        // If streaming to a temp file, return the file handle
        if ($asFile) {
            fseek($tempFile, 0);
            return $tempFile;
        }

        // Otherwise, return the JSON data
        return $returnData;
    }

    /**
     * Request via the POST method to return the result as a JSON data string.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return string
     */
    public function post($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'POST', $data);
    }

    /**
     * Request via the POST method to return the result as a temporary file
     * handle resource.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return resource
     */
    public function postToFile($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'POST', $data, true);
    }

    /**
     * Request via the PUT method to return the result as a JSON data string.
     *
     * @param string $path The request path.
     * @param array $data The request data.
     *
     * @return string
     */
    public function put($path, $data = null)
    {
        $data = is_array($data) ? $data : array();

        return $this->makeRequest($path, 'PUT', $data);
    }

    /**
     * Set the cURL configuration entity.
     *
     * @param \GHT\ApiClient\Entity\CurlConfigInterface
     */
    public function setCurlConfig(CurlConfigInterface $curlConfig)
    {
        $this->curlConfig = $curlConfig;
    }
}
