
GHT API Client
================================

The GHT API Client provides connectivity with API applications.

# Installation

To install with [Composer](https://getcomposer.org/), run `composer require greenhollowtech/ght-api-client`.

# Usage

To use the client:

```php
use GHT\ApiClient\Entity\ApiConnector;
use GHT\ApiClient\GHTApiClient;

// Store credentials in an untracked configuration file
$apiHost = 'https://sandbox.greenhollowtech.com';
$apiUser = 'example-user';
$apiKey = 'example-key';
$apiSecret = 'example-secret';
$connector = new ApiConnector($apiHost, $apiUser, $apiKey, $apiSecret);

// Instantiate the client using the authenticating connector
$sandboxMode = true;
$client = new GHTApiClient($connector, $sandboxMode);

// Make an API request
$response = $client->post('/api/example', array(
    'example' => 'example-request-value',
));

// The response will be a JSON-encoded string, so decode it
$response = json_decode($response, true);

// Other request methods
$response = $client->get('/api/example', array(
    'example' => 'example-request-value',
));

$response = $client->put('/api/example', array(
    'example' => 'example-put-value',
));

$response = $client->delete('/api/example', array(
    'example' => 'example-delete-value',
));

// Uploading files with PHP >= 5.5.0
$exampleFile = new \CURLFile('/tmp/example.png', mime_content_type('/tmp/example.png'));
$response = $client->put('/api/example', array(
    'example' => $exampleFile,
));

// Uploading files with PHP < 5.5.0
$response = $client->put('/api/example', array(
    'example' => '@/tmp/example.png;filename=example.png',
));

// Get the HTTP response code for the last request
$responseCode = $client->getResponseCode();
```

## OAuth2 Connections

You can also connect to an API using an OAuth2 token:

```php
use GHT\ApiClient\Entity\OauthConnector;
use GHT\ApiClient\GHTApiClient;

// Get the token from the target API's OAuth2 provider
$apiHost = 'https://sandbox.greenhollowtech.com';
$token = 'tokenProvidedByTheTargetApi';
$connector = new OauthConnector($apiHost, $token);

// Instantiate the client using the OAuth2 connector
$sandboxMode = true;
$client = new GHTApiClient($connector, $sandboxMode);
```

It's up to your application as to how it gets the token from the target API.  You might want to extend the OAuth2 connector class to include your own user identification and token expiration, then use that class to store the data.

## Handling Large Data Results

Response data can be streamed directly into a file to avoid loading the entire response into memory.

In the example below, `$responseFile` is a file handle resource of a temporary file containing the response data.  You can then write the file contents to the output buffer to provide a CSV download, for example, or rename the file to a permanent location.

```php
$responseFile = $client->getToFile('/api/example', array(
    'example' => 'example-request-value',
));

if ($responseFile !== false) {
    // Do something with $responseFile

    fclose($responseFile);
}
```

## Setting Custom cURL Options

Any cURL option can be set using the `CurlConfig` entity.  The options set by basic client inner workings are ignored (`CURLOPT_CUSTOMREQUEST`, `CURLOPT_HTTPHEADER`, `CURLOPT_POSTFIELDS`, and `CURLOPT_WRITEFUNCTION`).

```php
use GHT\ApiClient\Entity\CurlConfig;
...
$curlConfig = new CurlConfig(array(
    'connecttimeout' => 20,
    'timeout' => 20,
));
$client->setCurlConfig($curlConfig);
```
